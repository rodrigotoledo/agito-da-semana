# -*- encoding : utf-8 -*-
class CreateMagazines < ActiveRecord::Migration
  def self.up
    create_table :magazines do |t|
      t.string :title
      t.has_attached_file :photo
      t.text :description
      t.datetime :realized_at

      t.timestamps
    end
  end

  def self.down
    drop_table :magazines
  end
end
