class AddPhotographersToGalleries < ActiveRecord::Migration
  def self.up
    add_column :galleries, :photographers, :string
  end

  def self.down
    remove_column :galleries, :photographers
  end
end
