# -*- encoding : utf-8 -*-
class CreateBannerPositions < ActiveRecord::Migration
  def self.up
    create_table :banner_positions do |t|
      t.references :banner
      t.references :banner_place

      t.timestamps
    end
  end

  def self.down
    drop_table :banner_positions
  end
end
