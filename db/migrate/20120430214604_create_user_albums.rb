# -*- encoding : utf-8 -*-
class CreateUserAlbums < ActiveRecord::Migration
  def self.up
    create_table :user_albums do |t|
      t.references :user
      t.references :place
      t.references :event
      t.string :title
      t.references :user_album_image
      t.text :description
      t.string :address
      t.string :latitude
      t.string :longitude

      t.timestamps
    end
  end

  def self.down
    drop_table :user_albums
  end
end
