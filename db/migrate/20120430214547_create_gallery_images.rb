# -*- encoding : utf-8 -*-
class CreateGalleryImages < ActiveRecord::Migration
  def self.up
    create_table :gallery_images do |t|
      t.references :gallery
      t.has_attached_file :image

      t.timestamps
    end
  end

  def self.down
    drop_table :gallery_images
  end
end
