# -*- encoding : utf-8 -*-
class CreateNewsRelations < ActiveRecord::Migration
  def self.up
    create_table :news_relations do |t|
      t.references :news_category
      t.references :news

      t.timestamps
    end
  end

  def self.down
    drop_table :news_relations
  end
end
