# -*- encoding : utf-8 -*-
class CreateGalleryImageComments < ActiveRecord::Migration
  def self.up
    create_table :gallery_image_comments do |t|
      t.references :gallery_image
      t.references :user
      t.text :comment
      t.boolean :allow

      t.timestamps
    end
  end

  def self.down
    drop_table :gallery_image_comments
  end
end
