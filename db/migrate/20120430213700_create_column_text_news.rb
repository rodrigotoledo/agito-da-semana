# -*- encoding : utf-8 -*-
class CreateColumnTextNews < ActiveRecord::Migration
  def self.up
    create_table :column_text_news do |t|
      t.references :columnist
      t.string :title
      t.has_attached_file :photo
      t.string :short_description
      t.text :content

      t.timestamps
    end
  end

  def self.down
    drop_table :column_text_news
  end
end
