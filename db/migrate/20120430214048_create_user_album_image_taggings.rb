# -*- encoding : utf-8 -*-
class CreateUserAlbumImageTaggings < ActiveRecord::Migration
  def self.up
    create_table :user_album_image_taggings do |t|
      t.references :user
      t.references :user_album_image
      t.string :name
      t.integer :position_x
      t.integer :position_y

      t.timestamps
    end
  end

  def self.down
    drop_table :user_album_image_taggings
  end
end
