# -*- encoding : utf-8 -*-
class CreateGalleries < ActiveRecord::Migration
  def self.up
    create_table :galleries do |t|
      t.references :event
      t.references :place
      t.references :gallery_category
      t.integer :type_gallery
      t.integer :photographer_id
      t.string :title
      t.references :gallery_image
      t.text :description
      t.datetime :realized_at

      t.timestamps
    end
  end

  def self.down
    drop_table :galleries
  end
end
