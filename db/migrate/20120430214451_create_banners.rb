# -*- encoding : utf-8 -*-
class CreateBanners < ActiveRecord::Migration
  def self.up
    create_table :banners do |t|
      t.string :title
      t.integer :type_file
      t.has_attached_file :image
      t.has_attached_file :swf
      t.string :url

      t.timestamps
    end
  end

  def self.down
    drop_table :banners
  end
end
