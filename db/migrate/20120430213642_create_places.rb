# -*- encoding : utf-8 -*-
class CreatePlaces < ActiveRecord::Migration
  def self.up
    create_table :places do |t|
      t.string :name
      t.string :address
      t.string :latitude
      t.string :longitude
      t.text :description

      t.timestamps
    end
  end

  def self.down
    drop_table :places
  end
end
