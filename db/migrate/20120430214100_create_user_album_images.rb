# -*- encoding : utf-8 -*-
class CreateUserAlbumImages < ActiveRecord::Migration
  def self.up
    create_table :user_album_images do |t|
      t.references :user_album
      t.has_attached_file :image

      t.timestamps
    end
  end

  def self.down
    drop_table :user_album_images
  end
end
