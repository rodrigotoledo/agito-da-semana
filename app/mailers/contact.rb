# -*- encoding : utf-8 -*-
class Contact < ActionMailer::Base
  default :from => "contato@agitodasemana.com.br"

  # Subject can be set in your I18n file at config/locales/en.yml
  # with the following lookup:
  #
  #   en.contact.talk_to_us.subject
  #
  def talk_to_us(name,subject,email,phone,message)
    @name    = name
    @subject = subject
    @email   = email
    @phone   = phone
    @message = message

    mail :bcc => "agc.rodrigo@gmail.com", :to => "contato@agitodasemana.com.br", :reply_to => email, :subject => "Contato pelo site - #{subject}"
  end

  # Subject can be set in your I18n file at config/locales/en.yml
  # with the following lookup:
  #
  #   en.contact.15_years.subject
  #
  def to_15_years(name,email,phone,mobile,contact_name,address,house_number,neighborhood,city,realized_at,observations)
    @name         = name
    @email        = email
    @phone        = phone
    @mobile       = mobile
    @contact_name = contact_name
    @address      = address,
    @house_number = house_number
    @neighborhood = neighborhood
    @city         = city
    @realized_at  = realized_at
    @observations = observations

    mail :bcc => "agc.rodrigo@gmail.com", :to => "contato@agitodasemana.com.br", :reply_to => email, :subject => "Interesse de 15 anos"
  end
  
  def interest_event(name,flyer,email,phone,mobile,contact_name,address,house_number,neighborhood,city,realized_at,observations)
    @name         = name
    @email        = email
    @phone        = phone
    @mobile       = mobile
    @contact_name = contact_name
    @address      = address,
    @house_number = house_number
    @neighborhood = neighborhood
    @city         = city
    @realized_at  = realized_at
    @observations = observations
    
    attachments[flyer.original_filename] = flyer.read unless flyer.blank?

    mail :bcc => "agc.rodrigo@gmail.com", :to => "contato@agitodasemana.com.br", :reply_to => email, :subject => "Interesse de evento"
    
  end
end
