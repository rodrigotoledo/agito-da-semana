# -*- encoding : utf-8 -*-
class GalleryImage < ActiveRecord::Base
  belongs_to :gallery
  has_many :gallery_image_comments
  attr_accessible :image, :gallery_id
  has_attached_file :image, :styles => { 
    :medium => "300x300>", 
    :thumb => "75x75#", 
    :detail => '772'},
  :default_style => :detail

  validates_presence_of :gallery_id
  
  after_save :set_front_image
  
  def set_front_image
    return unless self.gallery.gallery_image_id.nil?
    self.set_front_image!
  end
  
  def set_front_image!
    self.gallery.update_attribute(:gallery_image_id, self.id)
  end
  
  def name
    self.image_file_name
  end
end
