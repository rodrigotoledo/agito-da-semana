# -*- encoding : utf-8 -*-
class News < ActiveRecord::Base
  default_scope order('created_at DESC')
  attr_accessible :content, :is_special, :photo, :short_description, :show_in_front, :title, :category_ids
  validates_presence_of :title,:short_description,:content
  has_many :news_relations
  has_many :categories, :through => :news_relations, :source => :news_category
  has_attached_file :photo, :styles => {:home => "370x152#", :big => "400"}
end
