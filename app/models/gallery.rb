# -*- encoding : utf-8 -*-
require 'rubygems'
require 'zip/zip'
require 'fileutils'

class Gallery < ActiveRecord::Base
  module Types
    COMMON, COLLYRIUM = 0, 1
  end
  default_scope order('realized_at DESC')
  scope :collyrium, where(:type_gallery => Gallery::Types::COLLYRIUM).where("image_front_file_name IS NOT NULL")
  scope :common, where(:type_gallery => Gallery::Types::COMMON).where("image_front_file_name IS NOT NULL")
  belongs_to :event
  belongs_to :place
  belongs_to :gallery_category
  belongs_to :gallery_image
  has_many :gallery_images, :dependent => :destroy
  has_many :gallery_comments, :through => :gallery_images, :source => :gallery_image_comments
  attr_accessor :folder_with_images
  attr_accessible :description, :photographers, :realized_at, :title, :type_gallery, :event_id, :place_id, :gallery_category_id, :compacted, :image_front, :folder_with_images
  has_attached_file :compacted
  
  # home: utilizada para imagem pequena na home
  # home_big: colirio na home
  # big: destaque na pagina da galeria
  has_attached_file :image_front, :styles => {:home => '238x92#', :home_big => '750x300', :big => "400x400>"}
  # 
  # validates_attachment_presence :image_front
  
  validates_presence_of :title, :realized_at, :description, :gallery_category, :type_gallery
  # after_save :save_images_in_folder
  
  def to_param
    [self.id,self.title.parameterize].join('-')
  end
  
  def save_images_in_folder
    unless self.folder_with_images.blank?
      directory_with_images = File.join(Rails.root,'fotos',self.folder_with_images,"*.jpg")
      files = Dir.glob(directory_with_images)
      files.each do |f_path|
        new_file_name = File.join(Rails.root,'fotos',self.folder_with_images,"#{self.to_param[0,10]}-#{Time.now.to_i}-#{File.basename(f_path)}")
        FileUtils.mv(f_path,new_file_name)
        GalleryImage.transaction do
          begin
            gallery_image = self.gallery_images.build(:image => File.open(new_file_name))
            gallery_image.save!
            File.unlink(new_file_name)
          rescue
            FileUtils.mv(new_file_name,f_path)
          end
        end
      end

      # tmp_gallery_path = "#{Rails.root}/tmp/gallery_#{self.id}/"
      # FileUtils.mkdir_p(tmp_gallery_path) unless File.directory?(tmp_gallery_path)
      # files = []
      # Zip::ZipFile.open(self.compacted.path) do |zip_file|
      #   zip_file.each do |f|
      #     f_path=File.join(tmp_gallery_path, f.name)
      #     FileUtils.mkdir_p(File.dirname(f_path))
      #     zip_file.extract(f, f_path) unless File.exist?(f_path)
      #     # File.unlink(File.join(tmp_gallery_path,f.name)) if File.exist?(File.join(tmp_gallery_path,f.name))
      #     # zip_file.extract(f.name,File.join(tmp_gallery_path,f.name)) 
      #     if File.file?(f_path)
      #       files << f_path
      #     end
      #   end
      # end
      # files.each do |f_path|
      #   gallery_image = self.gallery_images.build(:image => File.open(f_path))
      #   gallery_image.save
      # end
      # FileUtils.rm_rf(tmp_gallery_path)
    end
    true
  end
end
