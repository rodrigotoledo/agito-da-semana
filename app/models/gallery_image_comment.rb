# -*- encoding : utf-8 -*-
class GalleryImageComment < ActiveRecord::Base
  belongs_to :gallery_image
  belongs_to :user
  attr_accessible :allow, :comment, :user_id
end
