# -*- encoding : utf-8 -*-
class UserAlbumImageTagging < ActiveRecord::Base
  belongs_to :user
  belongs_to :user_album_image
  attr_accessible :name, :position_x, :position_y
  
  def self.create_from_js(params,current_user)
    user_album_image = UserAlbumImage.find(params[:image_id])
    raise 'sem autorizacao' if current_user.blank? || user_album_image.user_album.user_id != current_user.id
    
    user_in_tag = User.where(:id => params[:name_id]).first
    user_album_image_tagging                     = UserAlbumImageTagging.new
    user_album_image_tagging.user_album_image_id = user_album_image.id
    user_album_image_tagging.user_id             = user_in_tag.id if user_in_tag
    user_album_image_tagging.name                = user_in_tag ? user_in_tag.name : params[:name]
    user_album_image_tagging.position_x          = params[:left]
    user_album_image_tagging.position_y          = params[:top]
    user_album_image_tagging.save!
    {
      'result' => true,
      "tag" =>  {
        "id"             => user_album_image_tagging.id,
        "text"           => user_album_image_tagging.name,
        "left"           => user_album_image_tagging.position_x,
        "top"            => user_album_image_tagging.position_y,
        "width"          => 75,
        "height"         => 75,
        "isDeleteEnable" => true
      }
    }
  rescue => e
    {
      :result => false
    }
  end
  
  def self.find_all_to_js(user_album_image_id,current_user)
    tags = self.where(:user_album_image_id => user_album_image_id).all.collect do |t|
      {
        :id             => t.id,
        :text           => t.user ? t.user.name : t.name,
        :top            => t.position_y,
        :left           => t.position_x,
        :isDeleteEnable => current_user && t.user_album_image.user_album.user_id == current_user.id
      }
    end
    
    {
      'Image' => [
        {
        'id' => user_album_image_id,
        'Tags' => tags,
        }
      ],
      "options" => {
    		"literals" =>  {
    			"removeTag" => "remover"
    		},
    		"tag" => {
    			"flashAfterCreation" => true
    		}
    	}
    }
  end
end
