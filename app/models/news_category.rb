# -*- encoding : utf-8 -*-
class NewsCategory < ActiveRecord::Base
  default_scope order(:name)
  attr_accessible :name
  validates :name,  :presence => true, :uniqueness => true
end
