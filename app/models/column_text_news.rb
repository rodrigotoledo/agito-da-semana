# -*- encoding : utf-8 -*-
class ColumnTextNews < ActiveRecord::Base
  belongs_to :columnist
  attr_accessible :content, :photo, :short_description, :title, :columnist_id
  validates_presence_of :title, :content, :columnist_id
  has_attached_file :photo
end
