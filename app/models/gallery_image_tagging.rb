# -*- encoding : utf-8 -*-
class GalleryImageTagging < ActiveRecord::Base
  belongs_to :user
  belongs_to :gallery_image
  attr_accessible :name, :position_x, :position_y
end
