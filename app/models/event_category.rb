# -*- encoding : utf-8 -*-
class EventCategory < ActiveRecord::Base
  default_scope order(:name)
  attr_accessible :name
  validates_presence_of :name
  validates_uniqueness_of :name
end
