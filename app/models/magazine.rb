# -*- encoding : utf-8 -*-
class Magazine < ActiveRecord::Base
  default_scope order("realized_at DESC")
  
  attr_accessible :description, :photo, :realized_at, :title
  
  has_attached_file :photo, :styles => {:home => "370x152#", :big => "400"}
  
  validates_presence_of :title, :description, :realized_at
  validates_attachment_presence :photo
end
