# -*- encoding : utf-8 -*-
class AlbunsFotosController < ApplicationController
  layout 'ajax'
  
  # before_filter :tagging_paths_to_js
  
  def ver
    @user_album_image = UserAlbumImage.where(:user_album_id => params[:album_id],:id => params[:id]).first
    gon.pode_marcar   = true #owner?(@user_album_image.user_album.user)
  end
  
  def marcacoes
    render :json => UserAlbumImageTagging.find_all_to_js(params[:id],current_user)
  end
  
  def salvar_marcacao
    render :json => UserAlbumImageTagging.create_from_js(params,current_user)
  end
  
  def remover_marcacao
    user_album_image_tagging = UserAlbumImageTagging.where(:user_album_image_id => params[:id], :id => params[:tag_id]).first
    user_album_image_tagging.destroy if user_album_image_tagging && owner?(user_album_image_tagging.user_album_image.user_album.user)
    render :json => {:result => true,:message => "ok..."}
  rescue
    render :json => {:result => false,:message => "Sem autorização para esta ação"}
  end
  
  def tagging_paths_to_js
    gon.salvar_marcacao_path  = album_salvar_marcacao_path
    gon.remover_marcacao_path = album_remover_marcacao_path
    gon.marcacoes_path        = album_marcacoes_path
  end

end
