# -*- encoding : utf-8 -*-
class AgendasController < ApplicationController
  def index
    @events = Event.order("realized_at").paginate(:page => params[:page], :per_page => 12)
  end
  
  def detalhes
    @event = Event.find(params[:id])
  end
end
