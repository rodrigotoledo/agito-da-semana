# -*- encoding : utf-8 -*-
class AgitoController < ApplicationController
  def chame_agito
    
  end

  def fale_conosco
  end

  def index_15_anos
  end
  
  def enviar_chame_agito
    
    # raise params[:event][:flyer].inspect
    
    Contact.interest_event(
    params[:event][:name],
    params[:event][:flyer],
    params[:event][:email],
    params[:event][:phone],
    params[:event][:mobile],
    params[:event][:contact_name],
    params[:event][:address],
    params[:event][:house_number],
    params[:event][:neighborhood],
    params[:event][:city],
    params[:event][:realized_at],
    params[:event][:observations]).deliver
    redirect_to principal_path, :notice => 'Obrigado, sua mensagem será respondida em breve.'
  end
  
  def enviar_fale_conosco
    Contact.talk_to_us(params[:name],params[:subject],params[:email],params[:phone],params[:message]).deliver
    redirect_to principal_path, :notice => 'Obrigado, sua mensagem será respondida em breve.'
  end
  
  def enviar_15_anos
    Contact.to_15_years(
    params[:party][:name],
    params[:party][:email],
    params[:party][:phone],
    params[:party][:mobile],
    params[:party][:contact_name],
    params[:party][:address],
    params[:party][:house_number],
    params[:party][:neighborhood],
    params[:party][:city],
    params[:party][:realized_at],
    params[:party][:observations]).deliver
    redirect_to principal_path, :notice => 'Obrigado, sua mensagem será respondida em breve.'
  end
end
