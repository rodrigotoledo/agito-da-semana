# -*- encoding : utf-8 -*-
class ApplicationController < ActionController::Base
  protect_from_forgery
  
  helper_method :current_user, :logged?, :owner?, :yt_client
  
  def current_user
    return nil unless logged?
    @current_user ||= User.find(session[:current_user]['id'])
  end
  
  def check_logged
    return if controller_name == "login"
    unless logged?
      flash[:error] = t('global.not_logged_error')
      redirect_to entrar_path
    end
  end
  
  def logged?
    !session[:current_user].nil?
  end
  
  def owner?(user)
    return false unless logged?
    user.nil? || user.id == current_user.id
  end

  def yt_client
    @yt_client ||= YouTubeIt::Client.new(:username => YouTubeITConfig.username , :password => YouTubeITConfig.password , :dev_key => YouTubeITConfig.dev_key)
  end
  
  protected
  def redirect_if_logged
    redirect_to principal_path if logged?
  end
end
