# -*- encoding : utf-8 -*-
class GaleriasFotosController < ApplicationController
  layout 'ajax'
  def ver
    @gallery_image = GalleryImage.where(:gallery_id => params[:gallery_id],:id => params[:id]).first
  end

  def comentar
    unless logged?
      render :nothing => true
      return
    end
    @gallery_image = GalleryImage.where(:gallery_id => params[:gallery_id],:id => params[:id]).first
    @comment = @gallery_image.gallery_image_comments.build(:user_id => current_user.id, :comment => params[:comment])
    if @comment.save
      render :layout => nil
    else
      render :nothing => true
    end
  end

end
