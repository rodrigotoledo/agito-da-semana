# -*- encoding : utf-8 -*-
class VideosController < ApplicationController
  def index
    @videos = Video.scoped
    @videos = @videos.where(:video_category_id => params[:categoria]) if params[:categoria]
    
    @videos = @videos.paginate(:page => params[:page], :per_page => 3)
  end
  
  def detalhes
  end
end
