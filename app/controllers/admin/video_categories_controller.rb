# -*- encoding : utf-8 -*-
class Admin::VideoCategoriesController < Admin::AdminController
  # GET /admin/video_categories
  # GET /admin/video_categories.xml
  def index
    @video_categories = VideoCategory.paginate(:page => params[:page], :per_page => 10)

    respond_to do |format|
      format.html # index.html.erb
      format.xml  { render :xml => @video_categories }
    end
  end

  # GET /admin/video_categories/1
  # GET /admin/video_categories/1.xml
  def show
    @video_category = VideoCategory.find(params[:id])

    respond_to do |format|
      format.html # show.html.erb
      format.xml  { render :xml => @video_category }
    end
  end

  # GET /admin/video_categories/new
  # GET /admin/video_categories/new.xml
  def new
    @video_category = VideoCategory.new

    respond_to do |format|
      format.html # new.html.erb
      format.xml  { render :xml => @video_category }
    end
  end

  # GET /admin/video_categories/1/edit
  def edit
    @video_category = VideoCategory.find(params[:id])
  end

  # POST /admin/video_categories
  # POST /admin/video_categories.xml
  def create
    @video_category = VideoCategory.new(params[:video_category])

    respond_to do |format|
      if @video_category.save
        format.html { redirect_to(admin_video_categories_url, :notice => 'Video category was successfully created.') }
        format.xml  { render :xml => @video_category, :status => :created, :location => @video_category }
      else
        format.html { render :action => "new" }
        format.xml  { render :xml => @video_category.errors, :status => :unprocessable_entity }
      end
    end
  end

  # PUT /admin/video_categories/1
  # PUT /admin/video_categories/1.xml
  def update
    @video_category = VideoCategory.find(params[:id])

    respond_to do |format|
      if @video_category.update_attributes(params[:video_category])
        format.html { redirect_to(admin_video_categories_url, :notice => 'Video category was successfully updated.') }
        format.xml  { head :ok }
      else
        format.html { render :action => "edit" }
        format.xml  { render :xml => @video_category.errors, :status => :unprocessable_entity }
      end
    end
  end

  # DELETE /admin/video_categories/1
  # DELETE /admin/video_categories/1.xml
  def destroy
    @video_category = VideoCategory.find(params[:id])
    @video_category.destroy

    respond_to do |format|
      format.html { redirect_to(admin_video_categories_url) }
      format.xml  { head :ok }
    end
  end
end
