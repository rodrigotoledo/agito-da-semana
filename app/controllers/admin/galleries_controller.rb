# -*- encoding : utf-8 -*-
class Admin::GalleriesController < Admin::AdminController
  # GET /admin/galleries
  # GET /admin/galleries.json
  def index
    @galleries = Gallery.paginate(:page => params[:page], :per_page => 10)

    respond_to do |format|
      format.html # index.html.erb
      format.json { render :json => @galleries }
    end
  end

  # GET /admin/galleries/1
  # GET /admin/galleries/1.json
  def show
    @gallery = Gallery.find(params[:id])

    respond_to do |format|
      format.html # show.html.erb
      format.json { render :json => @gallery }
    end
  end

  # GET /admin/galleries/new
  # GET /admin/galleries/new.json
  def new
    @gallery = Gallery.new

    respond_to do |format|
      format.html # new.html.erb
      format.json { render :json => @gallery }
    end
  end

  # GET /admin/galleries/1/edit
  def edit
    @gallery = Gallery.find(params[:id])
  end

  # POST /admin/galleries
  # POST /admin/galleries.json
  def create
    @gallery = Gallery.new(params[:gallery])

    respond_to do |format|
      if @gallery.save
        @gallery.save_images_in_folder
        format.html { redirect_to edit_admin_gallery_path(@gallery), :notice => 'Gallery was successfully created.' }
        format.json { render :json => @gallery, :status => :created, :location => @gallery }
      else
        format.html { render :action => "new" }
        format.json { render :json => @gallery.errors, :status => :unprocessable_entity }
      end
    end
  end

  # PUT /admin/galleries/1
  # PUT /admin/galleries/1.json
  def update
    @gallery = Gallery.find(params[:id])

    respond_to do |format|
      if @gallery.update_attributes(params[:gallery])
        @gallery.save_images_in_folder
        format.html { redirect_to edit_admin_gallery_path(@gallery), :notice => 'Gallery was successfully updated.' }
        format.json { head :no_content }
      else
        format.html { render :action => "edit" }
        format.json { render :json => @gallery.errors, :status => :unprocessable_entity }
      end
    end
  end

  # DELETE /admin/galleries/1
  # DELETE /admin/galleries/1.json
  def destroy
    @gallery = Gallery.find(params[:id])
    @gallery.destroy

    respond_to do |format|
      format.html { redirect_to admin_galleries_url }
      format.json { head :no_content }
    end
  end
end
