# -*- encoding : utf-8 -*-
class Admin::AdminController < ApplicationController
  layout 'admin/application'
  before_filter :check_logged
  helper_method :current_user, :logged?
  
  def current_user
    @current_user ||= User.find(session[:admin_current_user]['id'])
  end
  
  def check_logged
    return if controller_name == "login"
    unless logged?
      flash[:error] = t('global.not_logged_error')
      redirect_to admin_login_path
    end
  end
  
  def logged?
    !session[:admin_current_user].nil?
  end
end
