# -*- encoding : utf-8 -*-
class Admin::PlacesController < Admin::AdminController
  # GET /admin/places
  # GET /admin/places.json
  def index
    @places = Place.paginate(:page => params[:page], :per_page => 10)

    respond_to do |format|
      format.html # index.html.erb
      format.json { render :json => @places }
    end
  end

  # GET /admin/places/1
  # GET /admin/places/1.json
  def show
    @place = Place.find(params[:id])

    respond_to do |format|
      format.html # show.html.erb
      format.json { render :json => @place }
    end
  end

  # GET /admin/places/new
  # GET /admin/places/new.json
  def new
    @place = Place.new

    respond_to do |format|
      format.html # new.html.erb
      format.json { render :json => @place }
    end
  end

  # GET /admin/places/1/edit
  def edit
    @place = Place.find(params[:id])
  end

  # POST /admin/places
  # POST /admin/places.json
  def create
    @place = Place.new(params[:place])

    respond_to do |format|
      if @place.save
        format.html { redirect_to [:admin,@place], :notice => 'Local criado com sucesso!' }
        format.json { render :json => @place, :status => :created, :location => @place }
      else
        flash[:error] = 'Erro ao criar o local'
        format.html { render :action => "new" }
        format.json { render :json => @place.errors, :status => :unprocessable_entity }
      end
    end
  end

  # PUT /admin/places/1
  # PUT /admin/places/1.json
  def update
    @place = Place.find(params[:id])

    respond_to do |format|
      if @place.update_attributes(params[:place])
        format.html { redirect_to [:admin,@place], :notice => 'Local atualizado com sucesso!' }
        format.json { head :no_content }
      else
        flash[:error] = 'Erro ao atualizar o local'
        format.html { render :action => "edit" }
        format.json { render :json => @place.errors, :status => :unprocessable_entity }
      end
    end
  end

  # DELETE /admin/places/1
  # DELETE /admin/places/1.json
  def destroy
    @place = Place.find(params[:id])
    @place.destroy

    respond_to do |format|
      format.html { redirect_to admin_places_url }
      format.json { head :no_content }
    end
  end
end
