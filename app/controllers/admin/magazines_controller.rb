# -*- encoding : utf-8 -*-
class Admin::MagazinesController < Admin::AdminController
  # GET /admin/magazines
  # GET /admin/magazines.xml
  def index
    @magazines = Magazine.paginate(:page => params[:page], :per_page => 10)

    respond_to do |format|
      format.html # index.html.erb
      format.xml  { render :xml => @magazines }
    end
  end

  # GET /admin/magazines/1
  # GET /admin/magazines/1.xml
  def show
    @magazine = Magazine.find(params[:id])

    respond_to do |format|
      format.html # show.html.erb
      format.xml  { render :xml => @magazine }
    end
  end

  # GET /admin/magazines/new
  # GET /admin/magazines/new.xml
  def new
    @magazine = Magazine.new

    respond_to do |format|
      format.html # new.html.erb
      format.xml  { render :xml => @magazine }
    end
  end

  # GET /admin/magazines/1/edit
  def edit
    @magazine = Magazine.find(params[:id])
  end

  # POST /admin/magazines
  # POST /admin/magazines.xml
  def create
    @magazine = Magazine.new(params[:magazine])

    respond_to do |format|
      if @magazine.save
        format.html { redirect_to(admin_magazines_path, :notice => 'Magazine was successfully created.') }
        format.xml  { render :xml => @magazine, :status => :created, :location => @magazine }
      else
        format.html { render :action => "new" }
        format.xml  { render :xml => @magazine.errors, :status => :unprocessable_entity }
      end
    end
  end

  # PUT /admin/magazines/1
  # PUT /admin/magazines/1.xml
  def update
    @magazine = Magazine.find(params[:id])

    respond_to do |format|
      if @magazine.update_attributes(params[:magazine])
        format.html { redirect_to(admin_magazines_path, :notice => 'Magazine was successfully updated.') }
        format.xml  { head :ok }
      else
        format.html { render :action => "edit" }
        format.xml  { render :xml => @magazine.errors, :status => :unprocessable_entity }
      end
    end
  end

  # DELETE /admin/magazines/1
  # DELETE /admin/magazines/1.xml
  def destroy
    @magazine = Magazine.find(params[:id])
    @magazine.destroy

    respond_to do |format|
      format.html { redirect_to(admin_magazines_url) }
      format.xml  { head :ok }
    end
  end
end
