# -*- encoding : utf-8 -*-
class Admin::VideosController < Admin::AdminController
  # GET /admin/videos
  # GET /admin/videos.json
  def index
    @videos = Video.paginate(:page => params[:page], :per_page => 10)

    respond_to do |format|
      format.html # index.html.erb
      format.json { render :json => @videos }
    end
  end

  # GET /admin/videos/1
  # GET /admin/videos/1.json
  def show
    @video = Video.find(params[:id])

    respond_to do |format|
      format.html # show.html.erb
      format.json { render :json => @video }
    end
  end

  # GET /admin/videos/new
  # GET /admin/videos/new.json
  def new
    @video = Video.new

    respond_to do |format|
      format.html # new.html.erb
      format.json { render :json => @video }
    end
  end

  # GET /admin/videos/1/edit
  def edit
    @video = Video.find(params[:id])
  end

  # POST /admin/videos
  # POST /admin/videos.json
  def create
    @video = Video.new(params[:video])

    respond_to do |format|
      if @video.save
        format.html { redirect_to admin_videos_path, :notice => 'Video was successfully created.' }
        format.json { render :json => @video, :status => :created, :location => @video }
      else
        format.html { render :action => "new" }
        format.json { render :json => @video.errors, :status => :unprocessable_entity }
      end
    end
  end

  # PUT /admin/videos/1
  # PUT /admin/videos/1.json
  def update
    @video = Video.find(params[:id])

    respond_to do |format|
      if @video.update_attributes(params[:video])
        format.html { redirect_to admin_videos_path, :notice => 'Video was successfully updated.' }
        format.json { head :no_content }
      else
        format.html { render :action => "edit" }
        format.json { render :json => @video.errors, :status => :unprocessable_entity }
      end
    end
  end

  # DELETE /admin/videos/1
  # DELETE /admin/videos/1.json
  def destroy
    @video = Video.find(params[:id])
    @video.destroy

    respond_to do |format|
      format.html { redirect_to admin_videos_url }
      format.json { head :no_content }
    end
  end
end
