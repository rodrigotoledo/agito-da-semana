# -*- encoding : utf-8 -*-
class Admin::GalleryImagesController < ApplicationController
  # GET /admin/gallery_images
  # GET /admin/gallery_images.json
  def index
    @gallery_images = GalleryImage.all

    respond_to do |format|
      format.html # index.html.erb
      format.json { render :json => @gallery_images }
    end
  end

  # GET /admin/gallery_images/1
  # GET /admin/gallery_images/1.json
  def show
    @gallery_image = GalleryImage.find(params[:id])

    respond_to do |format|
      format.html # show.html.erb
      format.json { render :json => @gallery_image }
    end
  end

  # GET /admin/gallery_images/new
  # GET /admin/gallery_images/new.json
  def new
    @gallery_image = GalleryImage.new

    respond_to do |format|
      format.html # new.html.erb
      format.json { render :json => @gallery_image }
    end
  end

  # GET /admin/gallery_images/1/edit
  def edit
    @gallery_image = GalleryImage.find(params[:id])
  end

  # POST /admin/gallery_images
  # POST /admin/gallery_images.json
  def create
    gallery = Gallery.find(params[:gallery_id])
    @gallery_image = gallery.gallery_images.build(params[:gallery_image])

    if @gallery_image.save
      render :json => { 
        :normal_path => @gallery_image.image.url.to_s , 
        :thumb_path => @gallery_image.image.url(:thumb).to_s , 
        :name => @gallery_image.image_file_name }, :content_type => 'text/html'
    else
      #todo handle error
      render :json => { :result => 'error'}, :content_type => 'text/html'
    end
  end

  # PUT /admin/gallery_images/1
  # PUT /admin/gallery_images/1.json
  def update
    @gallery_image = GalleryImage.find(params[:id])

    respond_to do |format|
      if @gallery_image.update_attributes(params[:gallery_image])
        format.html { redirect_to @gallery_image, :notice => 'Gallery image was successfully updated.' }
        format.json { head :no_content }
      else
        format.html { render :action => "edit" }
        format.json { render :json => @gallery_image.errors, :status => :unprocessable_entity }
      end
    end
  end

  # DELETE /admin/gallery_images/1
  # DELETE /admin/gallery_images/1.json
  def destroy
    @gallery_image = GalleryImage.find(params[:id].to_i)
    @gallery_image.destroy

    respond_to do |format|
      format.html { redirect_to edit_admin_gallery_path(params[:gallery_id]) }
      format.json { head :no_content }
    end
  end
  
  def make_special
    @gallery_image = GalleryImage.find(params[:id].to_i)
    @gallery_image.set_front_image!
    redirect_to edit_admin_gallery_path(@gallery_image.gallery), :notice => 'Foto de capa atualizada'
  end
end
