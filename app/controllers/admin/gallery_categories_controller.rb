# -*- encoding : utf-8 -*-
class Admin::GalleryCategoriesController < Admin::AdminController
  # GET /admin/gallery_categories
  # GET /admin/gallery_categories.xml
  def index
    @gallery_categories = GalleryCategory.paginate(:page => params[:page], :per_page => 10)

    respond_to do |format|
      format.html # index.html.erb
      format.xml  { render :xml => @gallery_categories }
    end
  end

  # GET /admin/gallery_categories/1
  # GET /admin/gallery_categories/1.xml
  def show
    @gallery_category = GalleryCategory.find(params[:id])

    respond_to do |format|
      format.html # show.html.erb
      format.xml  { render :xml => @gallery_category }
    end
  end

  # GET /admin/gallery_categories/new
  # GET /admin/gallery_categories/new.xml
  def new
    @gallery_category = GalleryCategory.new

    respond_to do |format|
      format.html # new.html.erb
      format.xml  { render :xml => @gallery_category }
    end
  end

  # GET /admin/gallery_categories/1/edit
  def edit
    @gallery_category = GalleryCategory.find(params[:id])
  end

  # POST /admin/gallery_categories
  # POST /admin/gallery_categories.xml
  def create
    @gallery_category = GalleryCategory.new(params[:gallery_category])

    respond_to do |format|
      if @gallery_category.save
        format.html { redirect_to(admin_gallery_categories_path, :notice => 'Gallery category was successfully created.') }
        format.xml  { render :xml => @gallery_category, :status => :created, :location => @gallery_category }
      else
        format.html { render :action => "new" }
        format.xml  { render :xml => @gallery_category.errors, :status => :unprocessable_entity }
      end
    end
  end

  # PUT /admin/gallery_categories/1
  # PUT /admin/gallery_categories/1.xml
  def update
    @gallery_category = GalleryCategory.find(params[:id])

    respond_to do |format|
      if @gallery_category.update_attributes(params[:gallery_category])
        format.html { redirect_to(admin_gallery_categories_path, :notice => 'Gallery category was successfully updated.') }
        format.xml  { head :ok }
      else
        format.html { render :action => "edit" }
        format.xml  { render :xml => @gallery_category.errors, :status => :unprocessable_entity }
      end
    end
  end

  # DELETE /admin/gallery_categories/1
  # DELETE /admin/gallery_categories/1.xml
  def destroy
    @gallery_category = GalleryCategory.find(params[:id])
    @gallery_category.destroy

    respond_to do |format|
      format.html { redirect_to(admin_gallery_categories_path) }
      format.xml  { head :ok }
    end
  end
end
