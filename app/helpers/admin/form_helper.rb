# -*- encoding : utf-8 -*-
module Admin::FormHelper
  
  def title(content)
    content_for :title do
      content
    end
  end
  
  def action_edit(path)
    link_to image_tag('icn_edit.png', :title => 'Editar'), path
  end
  
  def action_destroy(path)
    link_to image_tag('icn_trash.png', :title => 'Apagar'), path, :method => :delete, :confirm => 'Deseja prosseguir com a ação?', :title => :delete
  end
  
  def actions_common(object)
    content = [action_edit(url_for(:action => :edit, :id => object)),action_destroy(url_for([:admin,object]))]
    raw(content.join('&nbsp;'))
  end
  
  def button_new(text,path)
    render :partial => 'admin/shared/button_new', :locals => {:text => text, :path => path}
  end
  
  def breadcrumbs(items)
    items = [{:text => 'Início',:url => admin_root_path}]+items
    output = []
    items.each_with_index do |item,i|
      if i+1 == items.count
        output << content_tag(:a,item[:text],:class => 'current')
      else
        output << link_to(item[:text],item[:url])
      end
    end
    content_for :breadcrumb do 
      raw(output.join('<div class="breadcrumb_divider"></div>'))
    end
  end
end
