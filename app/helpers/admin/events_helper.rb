# -*- encoding : utf-8 -*-
module Admin::EventsHelper
  def events_choices
    Event.all.collect{|t| ["#{t.title} - #{l(t.realized_at,:format => :short)}",t.id]}
  end
end
