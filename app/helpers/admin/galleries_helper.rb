# -*- encoding : utf-8 -*-
module Admin::GalleriesHelper
  def image_folders
    all_files = Dir.entries(File.join(Rails.root,'fotos'))
    folders = []
    all_files.sort!
    all_files.each do |f|
      full_path = File.join(Rails.root,'fotos',f)
      next if f == '.' || f == '..' || !File.directory?(full_path)

      directory_with_images = File.join(Rails.root,'fotos',f,"*.jpg")
      files = Dir.glob(directory_with_images)
      folders << ["#{f} - #{files.size} fotos",f]
    end
    folders
  end
end
