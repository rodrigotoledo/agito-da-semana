# -*- encoding : utf-8 -*-
module ApplicationHelper
  def users_options_for_current
    users = User.order(:name).all.collect{|t| [t.name,t.id]}
    options_for_select(users,current_user.id)
  end
  
  def flash_messages
    output = []
    flash.each do |type,message|
      output << content_tag(:h4, message, :class => "alert_#{type}")
    end
    raw(output.join)
  end
  
  def top_menu
    output = []
    output << link_to('Principal',principal_path)
    output << link_to('Fotos',galerias_path)
    output << link_to('Agenda','https://www.facebook.com/agitooficial?fref=ts', target: '_blank')
    output << link_to('Garota da Semana',colirios_path)
    output << link_to('Vídeos',videos_path)
    output << link_to('Notícias',noticias_path)
    output << link_to('Chame o agito',chame_agito_path)
    output << link_to('15 anos', index_15_anos_path)
    output << link_to('Fale conosco', fale_conosco_path)
    # unless logged?
    #       output << link_to('Registrar', usuarios_registrar_path)
    #       output << link_to('Entrar', entrar_path)
    #     else
    #       output << link_to('Sair', usuarios_sair_path)
    #       output << link_to('Seus álbuns', albuns_path)
    #     end
    #     output << link_to('15 anos', index_15_anos_path)
    #     output << link_to('Fale conosco', fale_conosco_path)
    
    output = output.collect{|t| content_tag(:li,"&#187; #{t}".html_safe)}
    
    # content_tag(:ul,output.join(content_tag(:li,image_tag('top-menu-separator.jpg'),:class => 'separator')).html_safe)
    content_tag(:ul,output.join.html_safe)
  end
end
