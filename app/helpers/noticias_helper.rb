# -*- encoding : utf-8 -*-
module NoticiasHelper
  def last_2_news
    News.where(:show_in_front => true).limit(2)
  end
end
